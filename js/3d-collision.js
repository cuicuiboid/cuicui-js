import * as THREE from 'https://unpkg.com/three@0.126.1/build/three.module.js';
import { OrbitControls } from "./node_modules/three/examples/jsm/controls/OrbitControls.js";
import { GUI } from "./node_modules/three/examples/jsm/libs/dat.gui.module.js"
import Stats from './node_modules/three/examples/jsm/libs/stats.module.js';

// Set Principal Variable
var boid;
let obstacles = [];
let spawnZones = [];
let BoidsMeshGroup = new THREE.Group();
let ObstacleGroup = new THREE.Group();
let SpawnZoneGroup = new THREE.Group();
let selctionnedObstacle;
let selctionnedSpawnZone;
let AverageDistance = [];

const COLOR_OBSTACLE = 0x00c000;
const COLOR_OBSTACLE_SELECTED = 0xff0000;
const COLOR_SPAWN_ZONE = 0x08fffee;
const COLOR_SPAWN_ZONE_SELECTED = 0xffaa44;
const COLOR_BOID = 0x33aaff;



let gui = new GUI();

var option = {
    nbBoids: 500,
    size: 500,
    params: {
        maxSpeed: 10,
        align: {
            effectiveRange: 45,
            maxforce: 0.05
        },
        separate: {
            effectiveRange: 85,
            maxforce: 0.05
        },
        cohesion: {
            effectiveRange: 5,
            maxforce: 0.05
        }
    },
    collision: [],
    spawnZone: []
}

/* 
    Add gui Option 
*/



const divisions = 100;
var gridHelper = new THREE.GridHelper(option.size * 2, divisions);


function generateFolder() {
    gui.destroy()
    gui = new GUI()


    gui.add(option, "nbBoids", 0, 1000);
    gui.add(option, "size", 0, 1000).onChange(function (value) {
        generateGrid()
    });
    const folderBoid = gui.addFolder('Boid');
    folderBoid.add(option.params, "maxSpeed", 0, 500);
    const folderAlign = folderBoid.addFolder('Align');
    folderAlign.add(option.params.align, "effectiveRange", 0, 500);
    folderAlign.add(option.params.align, "maxforce", 0, 1);
    const folderSeparate = folderBoid.addFolder('Separate');
    folderSeparate.add(option.params.separate, "effectiveRange", 0, 500);
    folderSeparate.add(option.params.separate, "maxforce", 0, 1);
    const folderCohesion = folderBoid.addFolder('Cohesion');
    folderCohesion.add(option.params.cohesion, "effectiveRange", 0, 500);
    folderCohesion.add(option.params.cohesion, "maxforce", 0, 1);


    gui.add(guiFonction, "start")
    gui.add(guiFonction, "stop")
    gui.add(guiFonction, "exportName")
    gui.add(guiFonction, "download")
    gui.add(guiFonction, "downloadAverageDistance")
    gui.add(guiFonction, "addObstacle")
    gui.add(guiFonction, "addSpawnZone")
    gui.add(guiFonction, "unselect")
    ObjectFold = gui.addFolder(`obstacle unselected`)
    generateGrid()
}

function generateGrid() {
    scene.remove(gridHelper)
    gridHelper = new THREE.GridHelper(option.size * 2, divisions);
    gridHelper.position.y = - option.size;
    scene.add(gridHelper);
}


// Define Boid class to make it easier
const getRandomNum = (min = 0, max = 0) => Math.floor(Math.random() * (max + 1 - min)) + min;

class Boids {
    constructor(boids = []) {
        this.boids = boids
        this.params = option.params
    }

    update() {
        if (this.boids.length != 0) {
            let temp = this.boids.slice();
            temp.sort((a, b) => {
                this.boids[0].mesh.position.distanceTo(a.mesh.position) - this.boids[0].mesh.position.distanceTo(b.mesh.position)
            })
            AverageDistance.push([temp.slice(1, 5).reduce((a, b) => a + this.boids[0].mesh.position.distanceTo(b.mesh.position), 0)/5, temp.slice(1, 5).reduce((a, b) => a + b.velocity.length(), 0)/5])
            this.boids.forEach(boidcreature => {

                boidcreature.applyForce(this.align(boidcreature))

                boidcreature.applyForce(this.cohesion(boidcreature))

                boidcreature.applyForce(this.separate(boidcreature))
                boidcreature.applyForce(this.avoidBoxContainer(boidcreature))

                boidcreature.applyForce(this.avoidObstacle(boidcreature))
            })
            this.boids.forEach(boidcreature => {
                boidcreature.update()
            });
        }
    }

    align(boidcreature) {
        const sumVector = new THREE.Vector3()
        let nbBoidALignEffect = 0
        this.boids.forEach(otherBoid => {
            let distance = boidcreature.mesh.position.distanceTo(otherBoid.mesh.position)

            if (0 < distance && distance < this.params.align.effectiveRange) {
                sumVector.add(otherBoid.velocity)

                nbBoidALignEffect++;

            }


        })
        if (nbBoidALignEffect > 0) {
            sumVector.divideScalar(nbBoidALignEffect)
            sumVector.normalize()
            sumVector.multiplyScalar(this.params.maxSpeed)
        }
        if (sumVector.length() > this.params.align.maxForce) {
            sumVector.clampLength(0, this.params.align.maxForce)
        }
        return sumVector
    }


    cohesion(boidcreature) {
        const sumVector = new THREE.Vector3()
        let nbBoidcohesionEffect = 0
        this.boids.forEach(otherBoid => {
            let distance = boidcreature.mesh.position.distanceTo(otherBoid.mesh.position)
            if (0 < distance && distance < this.params.cohesion.effectiveRange) {
                sumVector.add(otherBoid.mesh.position);
                nbBoidcohesionEffect++

            }
        })

        if (nbBoidcohesionEffect > 0) {
            sumVector.divideScalar(nbBoidcohesionEffect)
            sumVector.subVectors(sumVector, boidcreature.mesh.position)
            sumVector.normalize()
            sumVector.multiplyScalar(this.params.maxSpeed)
            sumVector.subVectors(sumVector, boidcreature.velocity)
        }
        if (sumVector.length() > this.params.cohesion.maxForce) {
            sumVector.clampLength(0, this.params.cohesion.maxForce)
        }
        return sumVector
    }

    separate(boidcreature) {
        const sumVector = new THREE.Vector3()
        let nbBoidseperateEffect = 0
        this.boids.forEach(otherBoid => {
            let distance = boidcreature.mesh.position.distanceTo(otherBoid.mesh.position)

            if (0 < distance && distance < this.params.separate.effectiveRange) {
                let boidcreatureOtherBoidVector = new THREE.Vector3()
                boidcreatureOtherBoidVector.subVectors(boidcreature.mesh.position, otherBoid.mesh.position)
                boidcreatureOtherBoidVector.normalize();
                boidcreatureOtherBoidVector.divideScalar(distance);
                sumVector.add(boidcreatureOtherBoidVector)
            }
        })

        if (nbBoidseperateEffect > 0) {
            sumVector.divideScalar(nbBoidcohesionEffect)
            sumVector.normalize()
            sumVector.multiplyScalar(this.params.maxSpeed)
            sumVector.sub(boidcreature.velocity)
        }

        if (sumVector.length() > this.params.separate.maxForce) {
            sumVector.clampLength(0, this.params.separate.maxForce)
        }
        return sumVector
    }


    avoidBoxContainer(boidcreature) {
        const sumVector = new THREE.Vector3()

        if (boidcreature.mesh.position.x + boidcreature.velocity.x > option.size) {
            sumVector.x = -boidcreature.velocity.x
        }
        if (boidcreature.mesh.position.x + boidcreature.velocity.x < -option.size) {
            sumVector.x = -boidcreature.velocity.x
        }
        if (boidcreature.mesh.position.z + boidcreature.velocity.z > option.size) {
            sumVector.z = -boidcreature.velocity.z
        }
        if (boidcreature.mesh.position.z + boidcreature.velocity.z < -option.size) {
            sumVector.z = -boidcreature.velocity.z
        }
        if (boidcreature.mesh.position.y + boidcreature.velocity.y > option.size) {
            sumVector.y = -boidcreature.velocity.y
        }
        if (boidcreature.mesh.position.y + boidcreature.velocity.y < -option.size) {
            sumVector.y = -boidcreature.velocity.y
        }
        sumVector.normalize()
        sumVector.multiplyScalar(Math.pow(boidcreature.velocity.length(), 3));
        return sumVector
    }

    avoidObstacle(boidcreature) {
        const sumVector = new THREE.Vector3()
        const dir = boidcreature.velocity.clone()
        dir.normalize()
        const myRay = new THREE.Raycaster(boidcreature.mesh.position, dir)
        const intersect = myRay.intersectObjects(ObstacleGroup.children)

        if (intersect.length > 0) {
            dir.reflect(intersect[0].face.normal)
            sumVector.add(dir)
            sumVector.normalize()
            sumVector.multiplyScalar(Math.pow(boidcreature.velocity.length(), 1));
        }
        return sumVector
    }
}





class BoidCreature {

    // Initialize variable of boid

    constructor() {
        const geometry = new THREE.CylinderGeometry(1, 8, 25, 12);
        geometry.rotateX(THREE.Math.degToRad(90));
        const color = COLOR_BOID;
        const material = new THREE.MeshLambertMaterial({
            wireframe: false,
            color: color
        });
        this.mesh = new THREE.Mesh(geometry, material);

        if (option.spawnZone.length > 0) {
            const zone = option.spawnZone[getRandomNum(0, option.spawnZone.length - 1)]
            this.mesh.position.x = getRandomNum(-zone.width / 2, zone.width / 2) + zone.x
            this.mesh.position.y = getRandomNum(-zone.height / 2, zone.height / 2) + zone.y
            this.mesh.position.z = getRandomNum(-zone.depth / 2, zone.depth / 2) + zone.z
        } else {
            this.mesh.position.x = getRandomNum(-option.size, option.size);
            this.mesh.position.y = getRandomNum(-option.size, option.size);
            this.mesh.position.z = getRandomNum(-option.size, option.size);
        }
        this.velocity = new THREE.Vector3(
            getRandomNum(-100, 100) * 0.1,
            getRandomNum(-100, 100) * 0.1,
            getRandomNum(-100, 100) * 0.1
        );
        this.acceleration = new THREE.Vector3();
        this.wonderTheta = 0;
        this.maxSpeed = option.params.maxSpeed;
        this.boost = new THREE.Vector3();
        this.force = new THREE.Vector3();
    }

    applyForce(f) {
        this.acceleration.add(f.clone());
    }

    update() {
        const maxSpeed = option.params.maxSpeed;

        // boost
        this.applyForce(this.boost);
        this.boost.multiplyScalar(0.9);
        if (this.boost.length() < 0.01) {
            this.boost = new THREE.Vector3();
        }

        // update velocity
        this.velocity.add(this.acceleration);

        // limit velocity
        if (this.velocity.length() > maxSpeed) {
            this.velocity.clampLength(0, maxSpeed);
        }

        // update position
        this.mesh.position.add(this.velocity);

        // reset acc
        this.acceleration.multiplyScalar(0);

        // head
        const head = this.velocity.clone();
        head.multiplyScalar(10);
        head.add(this.mesh.position);
        this.mesh.lookAt(head);
    }

}



class Obstacle {

    constructor(width, height, depth, x, y, z) {
        const geometry = new THREE.BoxGeometry();
        const material = new THREE.MeshBasicMaterial({ color: COLOR_OBSTACLE });
        this.mesh = new THREE.Mesh(geometry, material);
        this.mesh.scale.x = width
        this.mesh.scale.y = height
        this.mesh.scale.z = depth
        this.mesh.position.x = x
        this.mesh.position.y = y
        this.mesh.position.z = z;
    };

}




// Init Scene for 3d
const scene = new THREE.Scene
const render = new THREE.WebGLRenderer({
    antialis: true,
    alpha: true,
});
const canv = document.getElementById("canv");

scene.add(new THREE.AxesHelper(20));
//Camera
const camera = new THREE.PerspectiveCamera(75, canv.offsetWidth / canv.offsetHeight, 0.001, 10000);
camera.position.z = 1000;

scene.add(camera);
/* 
    Control
*/
const control = new OrbitControls(camera, render.domElement);



render.setClearColor(0x000000, 0);
render.setSize(canv.offsetWidth, canv.offsetHeight);
render.setPixelRatio(Math.min(window.devicePixelRatio, 2));
document.getElementById("canv").appendChild(render.domElement);
render.render(scene, camera);

const light = new THREE.AmbientLight(0xF0F0F0); // soft white light
scene.add(light);


// Loop

function anim() {
    control.update();


    stats.begin();

    boid.update()

    render.render(scene, camera);
    stats.end();
    stats.update();
    //replay
    requestAnimationFrame(anim);

}

// Responsive resize
window.addEventListener('resize', () => {
    render.setPixelRatio(Math.min(window.devicePixelRatio, 2));
    render.setSize(canv.offsetWidth, canv.offsetHeight);
    camera.aspect = canv.offsetWidth / canv.offsetHeight;
    camera.updateProjectionMatrix();

})




const generateBoid = () => {
    const boids = [];
    BoidsMeshGroup = new THREE.Group();
    for (let i = 0; i < option.nbBoids; i++) {
        const boid = new BoidCreature();
        BoidsMeshGroup.add(boid.mesh);
        boids.push(boid);
    }
    boid = new Boids(boids);
    scene.add(BoidsMeshGroup);
}

canv.appendChild(render.domElement);
let stats = new Stats();
canv.appendChild(stats.dom);




var inputfile = document.getElementById("loadfile");
inputfile.addEventListener('change', function () {
    const reader = new FileReader();
    reader.onload = function (e) {

        option = JSON.parse(e.target.result);
        unselectSpawnZone()
        unselectObstacle()
        scene.remove(ObstacleGroup)
        scene.remove(SpawnZoneGroup)

        for (let index = 0; index < obstacles.length; index++) {

            const ob = obstacles[index]
            ObstacleGroup.remove(ob.mesh)
        }
        for (let index = 0; index < spawnZones.length; index++) {

            const ob = spawnZones[index]
            SpawnZoneGroup.remove(ob.mesh)
        }

        obstacles = []
        spawnZones = []

        option.collision.forEach(obstacle => {

            const ob = new Obstacle(obstacle.width, obstacle.height, obstacle.depth, obstacle.x, obstacle.y, obstacle.z)
            obstacles.push(ob)
            ObstacleGroup.add(ob.mesh)

        });
        option.spawnZone.forEach(spawnZone => {

            const ob = new Obstacle(spawnZone.width, spawnZone.height, spawnZone.depth, spawnZone.x, spawnZone.y, spawnZone.z)
            ob.mesh.material.color.set(COLOR_SPAWN_ZONE)
            spawnZones.push(ob)
            SpawnZoneGroup.add(ob.mesh)

        });
        generateFolder()
        scene.add(SpawnZoneGroup)
        scene.add(ObstacleGroup)
    }

    reader.readAsText(inputfile.files[0])

})

var guiFonction = {
    exportName: "test",
    download: function () {
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(option));
        var downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href", dataStr);
        downloadAnchorNode.setAttribute("download", this.exportName + ".json");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    },
    downloadAverageDistance: function () {
        let csvContent = "data:text/csv;charset=utf-8," 
    + AverageDistance.map(e => e.join(",")).join("\n");
    var encodedUri = encodeURI(csvContent);
var link = document.createElement("a");
link.setAttribute("href", encodedUri);
link.setAttribute("download",  this.exportName +".csv");
document.body.appendChild(link); // Required for FF

link.click();
link.remove();
    },
    addObstacle: function () {

        const ob = new Obstacle(100, 100, 100, 0, 0, 0)
        obstacles.push(ob)

        option.collision.push(
            {
                width: 100,
                height: 100,
                depth: 100,
                x: 0,
                y: 0,
                z: 0
            }
        )
        unselectSpawnZone()
        unselectObstacle()
        ObstacleGroup.add(ob.mesh);
        scene.add(ObstacleGroup)
        selectObstacle(option.collision.length - 1)

    },
    addSpawnZone: function () {
        const ob = new Obstacle(100, 100, 100, 0, 0, 0)
        spawnZones.push(ob)

        option.spawnZone.push(
            {
                width: 100,
                height: 100,
                depth: 100,
                x: 0,
                y: 0,
                z: 0
            }
        )
        unselectSpawnZone()
        unselectObstacle()
        SpawnZoneGroup.add(ob.mesh)
        scene.add(SpawnZoneGroup)
        ob.mesh.material.color.set(COLOR_SPAWN_ZONE)
        selectSpawnZone(option.spawnZone.length - 1)


    },

    index: -1,
    indexSpawnZone: -1,
    removeObstacle: function () {
        ObstacleGroup.remove(obstacles[this.index].mesh)
        obstacles.splice(this.index, 1)
        option.collision.splice(this.index, 1)

        unselectObstacle()
    },
    removeSpawnZone: function () {
        SpawnZoneGroup.remove(spawnZones[this.indexSpawnZone].mesh)
        spawnZones.splice(this.indexSpawnZone, 1)
        option.spawnZone.splice(this.indexSpawnZone, 1)

        unselectSpawnZone()
    },
    start: function () {
        scene.remove(SpawnZoneGroup)
        AverageDistance = []
        if (boid.boids.length == 0) generateBoid()
    },
    stop: function () {
        scene.remove(BoidsMeshGroup)
        scene.add(SpawnZoneGroup)
        boid.boids.forEach(element => {
            BoidsMeshGroup.remove(element.mesh)
        });
        boid.boids = []
    },
    unselect: function () {
        unselectObstacle()
        unselectSpawnZone()
    }
}







const mouse = new THREE.Vector2();

function onMouseMove(event) {

    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components

    mouse.x = (event.clientX / canv.offsetWidth) * 2 - 1;
    mouse.y = - (event.clientY / canv.offsetHeight) * 2 + 1;

}
render.domElement.addEventListener("click", onclick, true);
var raycaster = new THREE.Raycaster();

function onclick(event) {
    onMouseMove(event)
    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects(ObstacleGroup.children);
    if (intersects.length > 0) {
        const index = obstacles.findIndex(element => {
            return element.mesh == intersects[0].object
        })
        if (index != -1) {
            unselectSpawnZone()
            unselectObstacle()
            selectObstacle(index)
        } else {
            var intersects = raycaster.intersectObjects(SpawnZoneGroup.children);
            if (intersects.length > 0) {
                const inde = spawnZones.findIndex(element => {
                    return element.mesh == intersects[0].object

                })

                if (inde != -1) {
                    unselectSpawnZone()
                    unselectObstacle()
                    selectSpawnZone(inde)

                }
            }
        }
    } else {
        var intersects = raycaster.intersectObjects(SpawnZoneGroup.children);
        if (intersects.length > 0) {
            const inde = spawnZones.findIndex(element => {
                return element.mesh == intersects[0].object

            })

            if (inde != -1) {
                unselectSpawnZone()
                unselectObstacle()
                selectSpawnZone(inde)

            }
        }
    }
}



var ObjectFold


/*
    Bad implementation
*/

function selectObstacle(index) {
    if (selctionnedObstacle) {
        selctionnedObstacle.mesh.material.color.set(COLOR_OBSTACLE)
    }
    if (ObjectFold) {
        gui.removeFolder(ObjectFold)
        ObjectFold = gui.addFolder(`Obstacle ${index}`)
    }
    selctionnedObstacle = obstacles[index]
    selctionnedObstacle.mesh.material.color.set(COLOR_OBSTACLE_SELECTED)

    ObjectFold.add(option.collision[index], "width", 1, option.size * 2).onChange(function (value) {
        obstacles[index].mesh.scale.x = value
    })
    ObjectFold.add(option.collision[index], "height", 1, option.size * 2).onChange(function (value) {
        obstacles[index].mesh.scale.y = value
    })
    ObjectFold.add(option.collision[index], "depth", 1, option.size * 2).onChange(function (value) {
        obstacles[index].mesh.scale.z = value
    })
    ObjectFold.add(option.collision[index], "x", -option.size, option.size).onChange(function (value) {
        obstacles[index].mesh.position.x = value
    })
    ObjectFold.add(option.collision[index], "y", -option.size, option.size).onChange(function (value) {
        obstacles[index].mesh.position.y = value
    })
    ObjectFold.add(option.collision[index], "z", -option.size, option.size).onChange(function (value) {
        obstacles[index].mesh.position.z = value
    })
    guiFonction.index = index
    ObjectFold.add(guiFonction, "removeObstacle")
    ObjectFold.open()
}

function unselectObstacle() {
    if (selctionnedObstacle) {
        selctionnedObstacle.mesh.material.color.set(COLOR_OBSTACLE)
        selctionnedObstacle = null;
    }
    if (ObjectFold) {
        gui.removeFolder(ObjectFold)
        ObjectFold = gui.addFolder(`Object unselected`)
    }
    ObjectFold.close()
    guiFonction.index = -1
}


function selectSpawnZone(index) {
    if (selctionnedSpawnZone) {
        selctionnedSpawnZone.mesh.material.color.set(COLOR_SPAWN_ZONE)
    }
    if (ObjectFold) {
        gui.removeFolder(ObjectFold)
        ObjectFold = gui.addFolder(`Spawn Zone ${index}`)
    }
    selctionnedSpawnZone = spawnZones[index]
    selctionnedSpawnZone.mesh.material.color.set(COLOR_SPAWN_ZONE_SELECTED)

    ObjectFold.add(option.spawnZone[index], "width", 1, option.size * 2).onChange(function (value) {
        spawnZones[index].mesh.scale.x = value
    })
    ObjectFold.add(option.spawnZone[index], "height", 1, option.size * 2).onChange(function (value) {
        spawnZones[index].mesh.scale.y = value
    })
    ObjectFold.add(option.spawnZone[index], "depth", 1, option.size * 2).onChange(function (value) {
        spawnZones[index].mesh.scale.z = value
    })
    ObjectFold.add(option.spawnZone[index], "x", -option.size, option.size).onChange(function (value) {
        spawnZones[index].mesh.position.x = value
    })
    ObjectFold.add(option.spawnZone[index], "y", -option.size, option.size).onChange(function (value) {
        spawnZones[index].mesh.position.y = value
    })
    ObjectFold.add(option.spawnZone[index], "z", -option.size, option.size).onChange(function (value) {
        spawnZones[index].mesh.position.z = value
    })
    guiFonction.indexSpawnZone = index
    ObjectFold.add(guiFonction, "removeSpawnZone")
    ObjectFold.open()
}


function unselectSpawnZone() {
    if (selctionnedSpawnZone) {
        selctionnedSpawnZone.mesh.material.color.set(COLOR_SPAWN_ZONE)
        selctionnedSpawnZone = null;
    }
    if (ObjectFold) {
        gui.removeFolder(ObjectFold)
        ObjectFold = gui.addFolder(`Object unselected`)
    }
    ObjectFold.close()
    guiFonction.index = -1
}




boid = new Boids()
generateFolder()
anim();
