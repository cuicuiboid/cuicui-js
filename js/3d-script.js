import {
    AxesHelper,
    BoxBufferGeometry,
    BufferAttribute, 
    BufferGeometry, 
    PerspectiveCamera, 
    Points, 
    PointsMaterial, 
    Scene, 
    WebGLRenderer
} from "./node_modules/three/build/three.module.js";
import { OrbitControls } from "./node_modules/three/examples/jsm/controls/OrbitControls.js";
import {GUI} from "./node_modules/three/examples/jsm/libs/dat.gui.module.js"
// Init Scene for 3d

const scene = new Scene
const render = new WebGLRenderer({
    antialis: true,
    alpha: true,
});
const canv = document.getElementById("canv");

scene.add(new AxesHelper(20));
//Camera
const camera = new PerspectiveCamera(75, canv.offsetWidth / canv.offsetHeight, 0.001, 10000);
camera.position.z = 500;

scene.add(camera);
// Control
const control = new OrbitControls(camera, render.domElement);

// Init Boids





var parameter = {
    nbBoids : 500,
    visualRange: 5625,
    speedLimit: 100,
    matchingFactor: 0.05,
};

var Boids = [];

for (var i = 0; i < parameter["nbBoids"]; i += 1) {
    Boids[Boids.length] = {
        x: Math.fround( Math.random() * 1000 - 500),
        y: Math.fround( Math.random() * 1000 - 500),
        z: Math.fround( Math.random() * 1000 - 500),
        dx:Math.fround( Math.random() * 10 - 5),
        dy:Math.fround( Math.random() * 10 - 5),
        dz:Math.fround( Math.random() * 10 - 5),
    };
}

console.log(Boids);
// Creation array for 3d scene 
let BirdPoints = new Float32Array(parameter["nbBoids"] * 3);

for (let i = 0; i < parameter["nbBoids"]; i++) {
    BirdPoints[3 * i] = Boids[i].x;
    BirdPoints[3 * i + 2] = Boids[i].y;
    BirdPoints[3 * i + 1] = Boids[i].z;
}

console.log(BirdPoints);


const geometry = new BufferGeometry();
geometry.setAttribute("position", new BufferAttribute(BirdPoints, 4));

const cube = new BoxBufferGeometry(500, 500, 500);

console.log(geometry);

const BirdMaterial = new PointsMaterial({
    color: 0xff0000,
    size: 10,
    sizeAttenuation: true,
});

let BirdObject = new Points(geometry, BirdMaterial);
const ponct = new Points(cube, new PointsMaterial({ color: 0x00ff00, size: 15, sizeAttenuation: false }));
scene.add(BirdObject);
scene.add(ponct);

render.setClearColor(0x000000, 0);
render.setSize(canv.offsetWidth, canv.offsetHeight);
render.setPixelRatio(Math.min(window.devicePixelRatio, 2));
document.getElementById("canv").appendChild(render.domElement);
render.render(scene, camera);

// Loop

function anim() {
    
    //Update boids and visual
    for (let boid of Boids) {
        flyTowardsCenter(boid);
        avoidOthers(boid);
        matchVelocity(boid);
        limitSpeed(boid);
        keepWithinBounds(boid);

        boid.x += boid.dx;
        boid.y += boid.dy;
        boid.z += boid.dz;

    }
    
    for (let i = 0; i < parameter["nbBoids"]; i++) {
        BirdPoints[3 * i] = Boids[i].x;
        BirdPoints[3 * i + 1] = Boids[i].y;
        BirdPoints[3 * i + 2] = Boids[i].z;
    }
    //Update Visual
    BirdObject.geometry.setAttribute("position", new BufferAttribute(BirdPoints, 3));
    BirdObject.geometry.attributes.position.needsUpdate = true;
    control.update();
    render.render(scene, camera);
    //relaunch
    requestAnimationFrame(anim);
}

// Responsive resize
window.addEventListener('resize', () => {
    camera.aspect = canv.offsetWidth / canv.offsetHeight ;
    camera.updateProjectionMatrix() ;
    render.setSize(canv.offsetWidth, canv.offsetHeight) ;
    render.setPixelRatio(Math.min(window.devicePixelRatio, 2)) ;
})


anim();


function distance(boid1, boid2) {
    return (boid1.x - boid2.x) * (boid1.x - boid2.x) + (boid1.y - boid2.y) * (boid1.y - boid2.y) + (boid1.z - boid2.z) * (boid1.z - boid2.z) ;
}

function nClosestBoids(boid, n) {
    // Make a copy
    const sorted = Boids.slice();
    // Sort the copy by distance from boid
    sorted.sort((a, b) => distance(boid, a) - distance(boid, b));
    // Return the n closest
    return sorted.slice(1, n + 1);
}

function keepWithinBounds(boid) {
    const margin = 50;
    const turnFactor = 1;

    if (boid.x < -500 + margin) {
        boid.dx += turnFactor;
    }
    if (boid.x > 500 - margin) {
        boid.dx -= turnFactor;
    }
    if (boid.y < -500 + margin) {
        boid.dy += turnFactor;
    }
    if (boid.y > 500 - margin) {
        boid.dy -= turnFactor;
    }
    if (boid.z < -500 + margin) {
        boid.dz += turnFactor;
    }
    if (boid.z > 500 - margin) {
        boid.dz -= turnFactor;
    }
}

// Find the center of mass of the other boids and adjust velocity slightly to
// point towards the center of mass.
function flyTowardsCenter(boid) {
    const centeringFactor = 0.005; // adjust velocity by this %

    let centerX = 0;
    let centerY = 0;
    let centerZ = 0;
    let numNeighbors = 0;

    for (let otherBoid of Boids) {
        if (distance(boid, otherBoid) < parameter["visualRange"]) {
            centerX += otherBoid.x;
            centerY += otherBoid.y;
            centerZ += otherBoid.z;
            numNeighbors += 1;
        }
    }

    if (numNeighbors) {
        centerX = centerX / numNeighbors;
        centerY = centerY / numNeighbors;
        centerZ = centerZ / numNeighbors;
        boid.dx += (centerX - boid.x) * centeringFactor;
        boid.dy += (centerY - boid.y) * centeringFactor;
        boid.dz += (centerZ - boid.z) * centeringFactor;
    }
}

// Move away from other boids that are too close to avoid colliding
function avoidOthers(boid) {
    const minDistance = 400; // The distance to stay away from other boids
    const avoidFactor = 0.05; // Adjust velocity by this %
    
    let moveX = 0;
    let moveY = 0;
    let moveZ = 0;
    
    for (let otherBoid of Boids) {
        if (otherBoid !== boid) {
            if (distance(boid, otherBoid) < minDistance) {
                moveX += boid.x - otherBoid.x;
                moveY += boid.y - otherBoid.y;
                moveZ += boid.z - otherBoid.z;
            }
        }
    }

    boid.dx += moveX * avoidFactor;
    boid.dy += moveY * avoidFactor;
    boid.dz += moveZ * avoidFactor;
}

// Find the average velocity (speed and direction) of the other boids and
// adjust velocity slightly to match.
function matchVelocity(boid) {

    let avgDX = 0;
    let avgDY = 0;
    let avgDZ = 0;
    let numNeighbors = 0;

    for (let otherBoid of Boids) {
        if (distance(boid, otherBoid) < parameter["visualRange"]) {
            avgDX += otherBoid.dx;
            avgDY += otherBoid.dy;
            avgDZ += otherBoid.dz;
            numNeighbors += 1;
        }
    }

    if (numNeighbors) {
        avgDX = avgDX / numNeighbors;
        avgDY = avgDY / numNeighbors;
        avgDZ = avgDZ / numNeighbors;
        boid.dx += (avgDX - boid.dx) * parameter["matchingFactor"];
        boid.dy += (avgDY - boid.dy) * parameter["matchingFactor"];
        boid.dz += (avgDZ - boid.dz) * parameter["matchingFactor"];
    }
}

// Speed will naturally vary in flocking behavior, but real animals can't go
// arbitrarily fast.
function limitSpeed(boid) {
    

    const SquaredSpeed = boid.dx * boid.dx + boid.dy * boid.dy + boid.dz * boid.dz;
    if (SquaredSpeed > parameter["speedLimit"]) {
        boid.dx = (boid.dx / SquaredSpeed) * parameter["speedLimit"];
        boid.dy = (boid.dy / SquaredSpeed) * parameter["speedLimit"];
        boid.dz = (boid.dz / SquaredSpeed) * parameter["speedLimit"];
    }
}

const gui = new GUI;
const menu = gui
menu.add(parameter,"nbBoids",0,1000);
menu.add(parameter,"visualRange",0,10000);
menu.add(parameter,"speedLimit", 0 , 1000);
menu.add(parameter, "matchingFactor",0,1);
