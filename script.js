// Size of canvas. These get updated to fill the whole browser.
let width = 550;
let height = 1224;

var numBoids = 200;
var visualRange = 5625;
var speedLimit = 100;

var boids = [];


function initBoids() {
  for (var i = 0; i < numBoids; i += 1) {
    boids[boids.length] = {
      x: Math.random() * width,
      y: Math.random() * height,
      dx: Math.random() * 10 - 5,
      dy: Math.random() * 10 - 5,
      history: [],
    };
  }
}

function distance(boid1, boid2) {
  return (boid1.x - boid2.x) * (boid1.x - boid2.x) + (boid1.y - boid2.y) * (boid1.y - boid2.y)
  ;
}

function nClosestBoids(boid, n) {
  // Make a copy
  const sorted = boids.slice();
  // Sort the copy by distance from boid
  sorted.sort((a, b) => distance(boid, a) - distance(boid, b));
  // Return the n closest
  return sorted.slice(1, n + 1);
}

// size canvas
function sizeCanvas() {
  const canvas = document.getElementById("boids");
  const canvas2 = document.getElementById("boidsMap");
  
  canvas.style.width ='100%';
  canvas.style.height='100%';
  // set the internal size 
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
  canvas2.style.width ='100%';
  canvas2.style.height='100%';
  canvas2.width  = canvas2.offsetWidth;
  canvas2.height = canvas2.offsetHeight;
  width = canvas.width;
  height = canvas.height;
}

// Constrain a boid to within the window.
function keepWithinBounds(boid) {
  const margin = 50;
  const turnFactor = 1;

  if (boid.x < margin) {
    boid.dx += turnFactor;
  }
  if (boid.x > width - margin) {
    boid.dx -= turnFactor
  }
  if (boid.y < margin) {
    boid.dy += turnFactor;
  }
  if (boid.y > height - margin) {
    boid.dy -= turnFactor;
  }
}

// Find the center of mass of the other boids and adjust velocity slightly to
// point towards the center of mass.
function flyTowardsCenter(boid) {
  const centeringFactor = 0.005; // adjust velocity by this %

  let centerX = 0;
  let centerY = 0;
  let numNeighbors = 0;

  for (let otherBoid of boids) {
    if (distance(boid, otherBoid) < visualRange) {
      centerX += otherBoid.x;
      centerY += otherBoid.y;
      numNeighbors += 1;
    }
  }

  if (numNeighbors) {
    centerX = centerX / numNeighbors;
    centerY = centerY / numNeighbors;

    boid.dx += (centerX - boid.x) * centeringFactor;
    boid.dy += (centerY - boid.y) * centeringFactor;
  }
}

// Move away from other boids that are too close to avoid colliding
function avoidOthers(boid) {
  const minDistance = 400; // The distance to stay away from other boids
  const avoidFactor = 0.05; // Adjust velocity by this %
  let moveX = 0;
  let moveY = 0;
  for (let otherBoid of boids) {
    if (otherBoid !== boid) {
      if (distance(boid, otherBoid) < minDistance) {
        moveX += boid.x - otherBoid.x;
        moveY += boid.y - otherBoid.y;
      }
    }
  }

  boid.dx += moveX * avoidFactor;
  boid.dy += moveY * avoidFactor;
}

// Find the average velocity (speed and direction) of the other boids and
// adjust velocity slightly to match.
function matchVelocity(boid) {
  const matchingFactor = 0.05; // Adjust by this % of average velocity

  let avgDX = 0;
  let avgDY = 0;
  let numNeighbors = 0;

  for (let otherBoid of boids) {
    if (distance(boid, otherBoid) < visualRange) {
      avgDX += otherBoid.dx;
      avgDY += otherBoid.dy;
      numNeighbors += 1;
    }
  }

  if (numNeighbors) {
    avgDX = avgDX / numNeighbors;
    avgDY = avgDY / numNeighbors;

    boid.dx += (avgDX - boid.dx) * matchingFactor;
    boid.dy += (avgDY - boid.dy) * matchingFactor;
  }
}

// Speed will naturally vary in flocking behavior, but real animals can't go
// arbitrarily fast.
function limitSpeed(boid) {
  const SquaredSpeed = boid.dx * boid.dx + boid.dy * boid.dy;
  
  if (SquaredSpeed > speedLimit) {
    boid.dx = (boid.dx / SquaredSpeed) * speedLimit;
    boid.dy = (boid.dy / SquaredSpeed) * speedLimit;
  }
}








var DRAW_TRAIL = true;

var DRAW_MASK = false;


function drawBoid(ctx,ctx2, boid) {
  const angle = Math.atan2(boid.dy, boid.dx);
  ctx.translate(boid.x, boid.y);
  ctx.rotate(angle);
  ctx.translate(-boid.x, -boid.y);
  ctx.fillStyle = "#558cf4";
  ctx.beginPath();
  ctx.moveTo(boid.x, boid.y);
  ctx.lineTo(boid.x - 15, boid.y + 5);
  ctx.lineTo(boid.x - 15, boid.y - 5);
  ctx.lineTo(boid.x, boid.y);
  ctx.fill();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  if (DRAW_MASK){
  ctx2.fillStyle = 'rgba(0,0,0,0.001)';
  ctx2.beginPath();
  ctx2.arc(boid.x,boid.y,1,0,Math.PI*2);
  ctx2.stroke();
  }
  if (DRAW_TRAIL) {
    ctx.strokeStyle = "#558cf466";
    ctx.beginPath();
    ctx.moveTo(boid.history[0][0], boid.history[0][1]);
    for (const point of boid.history) {
      ctx.lineTo(point[0], point[1]);
    }
    ctx.stroke();
  }


}


function animationLoop() {
  for (let boid of boids) {
    flyTowardsCenter(boid);
    avoidOthers(boid);
    matchVelocity(boid);
    limitSpeed(boid);
    keepWithinBounds(boid);

    boid.x += boid.dx;
    boid.y += boid.dy;
    boid.history.push([boid.x, boid.y])
    boid.history = boid.history.slice(-50);
  
  }


  const ctx = document.getElementById("boids").getContext("2d");
  const ctx2 = document.getElementById("boidsMap").getContext("2d");
  ctx.clearRect(0, 0, width, height);
  for (let boid of boids) {
    drawBoid(ctx,ctx2, boid);
  }


  window.requestAnimationFrame(animationLoop);
}

window.onload = () => {

  window.addEventListener("resize", sizeCanvas, false);

  sizeCanvas();
  

  ctx = document.getElementById("boids").getContext("2d");
  ctx2 =  document.getElementById("boidsMap").getContext("2d")
  initBoids();


  window.requestAnimationFrame(animationLoop);
};


var VisualRangeSlider = document.querySelector("#VisualRangeSlider")
VisualRangeSlider.addEventListener("change", (e)=> {
  visualRange = VisualRangeSlider.value;
});
VisualRangeSlider.addEventListener("input", ()=>{
  document.querySelector("#VisualRangeText").innerHTML = `Visual Range : ${VisualRangeSlider.value}`;
})


var BoidNumberSlider = document.querySelector("#BoidNumberSlider")
BoidNumberSlider.addEventListener("change", (e)=> {

});
BoidNumberSlider.addEventListener("input", ()=>{
  document.querySelector("#BoidNumberText").innerHTML = `Boid Number : ${BoidNumberSlider.value}`;
})

var SpeedSlider = document.querySelector("#SpeedLimitSlider")
SpeedSlider.addEventListener("change", (e)=> {
  speedLimit = SpeedSlider.value;
});
SpeedSlider.addEventListener("input", ()=>{
  document.querySelector("#SpeedLimitText").innerHTML = `Speed Limit : ${SpeedSlider.value}`;
})
